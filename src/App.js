import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Drawer from "./components/Drawer";
import Partners from "./Pages/Partners";
import Locations from "./Pages/Locations";
import CompanyForms from "./Pages/CompanyForms";

function App() {
  return (
    <Router>
      <Drawer>
        <Switch>
          <Route path="/locations" component={Locations} />
          <Route path="/companyForms" component={CompanyForms} />
          {/* Ha nincs megfelelő route, akkor a partnerlista jelenjen meg */}
          <Route component={Partners} />
        </Switch>
      </Drawer>
    </Router>
  );
}

export default App;
