import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { Business, LocationOn, Class } from "@material-ui/icons";
import { useHistory } from "react-router-dom";

// Drawer menu items
function Items() {
  // Item definitions
  const items = [
    { text: "Partnerek", icon: <Business />, route: "/" },
    { text: "Települések", icon: <LocationOn />, route: "/locations" },
    { text: "Cégformák", icon: <Class />, route: "/companyForms" }
  ];

  const history = useHistory();

  // Rendering drawer menu items
  return (
    <List>
      {items.map((item, key) => {
        return (
          <ListItem button key={key} onClick={() => history.push(item.route)}>
            <ListItemIcon>{item.icon}</ListItemIcon>
            <ListItemText primary={item.text} />
          </ListItem>
        );
      })}
    </List>
  );
}

export default Items;
