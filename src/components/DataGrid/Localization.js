// Localization defs for MaterialTable component
const tableLocalization = {
  pagination: {
    labelDisplayedRows: "{from}-{to} of {count}",
    labelRowsSelect: "sor"
  },
  toolbar: {
    nRowsSelected: "{0} row(s) selected",
    searchTooltip: "Keresés",
    searchPlaceholder: "Keresés"
  },
  header: {
    actions: "Műveletek"
  },
  body: {
    addTooltip: "Hozzáad",
    deleteTooltip: "Töröl",
    editTooltip: "Szerkesztés",
    emptyDataSourceMessage: "Nincs megjeleníthető adat",
    filterRow: { filterTooltip: "Szűrés" },
    editRow: {
      saveTooltip: "Mentés",
      cancelTooltip: "Mégsem",
      deleteText: "Valóban törli ezt a rekordot?"
    }
  }
};

export default tableLocalization;
