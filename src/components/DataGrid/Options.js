// Render Options attribute for MaterialTable grid component
const renderOptions = (rowSelect, selectedRow, filtering) => {
  return {
    filtering: filtering,
    actionsColumnIndex: -1,
    rowStyle: rowData => ({
      // Style for select row
      backgroundColor: selectedRow === rowData.tableData.id ? "#EEE" : "#FFF"
    }),
    exportButton: rowSelect ? false : true,
    exportDelimiter: ";"
  };
};

export default renderOptions;
