import React, { useEffect, useState } from "react";
import MaterialTable from "material-table";
import TableIcons from "./Icons"; // Get icons from exteral component
import useData from "./Hooks/useData";
import useColumns from "./Hooks/useColumns";
import { withSnackbar } from "notistack";
import Localization from "./Localization"; // Get localization deft from external file
import renderActions from "./Actions";
import renderOptions from "./Options";

function DataGrid(props) {
  // HOOK to store and manage grid data
  const [data, updateDataRow] = useData(props.dataRoute);

  // HOOK to render column definition
  const [columns] = useColumns(props.columns);

  // Loading state for the grid
  const [loading, setLoading] = useState(true);

  // Store row data if it is selected
  const [selectedRow, setSelectedRow] = useState(null);

  // Filtering option is enabled or disabled
  const [filtering, setFiltering] = useState(false);

  // Rendering options attribute
  const options = renderOptions(props.onRowSelect, selectedRow, filtering);

  // Rendering actions attribute
  const actions = renderActions(props.onRowSelect, () =>
    setFiltering(!filtering)
  );

  /**
   * When the column definition is not rendered and the dataset is not available,
   * show loading circle. When all data is available, disable it and show the grid.
   * */
  useEffect(() => {
    if (data && columns) setLoading(false);
  }, [data, columns]);

  // Change row of the data (GRID and DB) and send message for the user
  const onChangeRow = (mode, params) =>
    new Promise((resolve, reject) => {
      updateDataRow(mode, params)
        .then(result => {
          resolve();
          props.enqueueSnackbar(result, { variant: "success" });
        })
        .catch(error => {
          reject();
          props.enqueueSnackbar(error, { variant: "error" });
        });
    });

  // Event function when a row selected in the grid
  const onRowSelect = (evt, selectedRow) => {
    setSelectedRow(selectedRow.tableData.id);
    props.onRowSelect(selectedRow);
  };

  return (
    <MaterialTable
      {...props}
      localization={Localization}
      icons={TableIcons}
      data={data ? data : []}
      columns={columns ? columns : []}
      isLoading={loading}
      onRowClick={props.onRowSelect ? onRowSelect : null}
      options={options}
      editable={{
        onRowAdd: newData => onChangeRow("add", { newData }),
        onRowUpdate: (newData, oldData) =>
          onChangeRow("update", { newData, oldData }),
        onRowDelete: oldData => onChangeRow("delete", { oldData })
      }}
      actions={actions}
    />
  );
}

export default withSnackbar(DataGrid);
