import { useEffect, useState } from "react";
import axios from "axios";

// HOOK for store and manage dataset for the grid component

function useData(dataRoute) {
  const [data, setData] = useState(null);

  // Initial loading of data and store in state
  useEffect(() => {
    axios
      .get(process.env.REACT_APP_REST_URL + dataRoute)
      .then(result => setData(result.data))
      .catch(console.log);
  }, [dataRoute]);

  // When a row is inserted, REST call to insert it to DB and update data state
  const addRow = params => {
    return new Promise((resolve, reject) => {
      axios
        .post(process.env.REACT_APP_REST_URL + dataRoute, params.newData)
        .then(result => {
          setData([result.data, ...data]);
          resolve("Sikeres felvitel.");
        })
        .catch(error => {
          reject("Hiba! Sikertelen adatfelvitel.");
        });
    });
  };

  // When a row is updated, REST call to update it in DB and update data state
  const updateRow = params => {
    return new Promise((resolve, reject) => {
      axios
        .put(process.env.REACT_APP_REST_URL + dataRoute, params.newData)
        .then(result => {
          setData(
            data.map(dataElement => {
              if (dataElement.id === result.data.id) {
                return result.data;
              } else {
                return dataElement;
              }
            })
          );
          resolve("Sikeres módosítás.");
        })
        .catch(error => {
          reject("Hiba! Az adatok módosítása sikertelen.");
        });
    });
  };

  // When a row is deleted, REST call to delete is from DB and update data state
  const deleteRow = params => {
    return new Promise((resolve, reject) => {
      axios
        .delete(
          process.env.REACT_APP_REST_URL + dataRoute + "/" + params.oldData.id
        )
        .then(() => {
          setData(
            data.filter(dataElement => {
              if (dataElement.id !== params.oldData.id) {
                return dataElement;
              } else {
                return null;
              }
            })
          );
          resolve("Sikeres törlés.");
        })
        .catch(error => {
          reject("Hiba! Sikertelen törlés.");
        });
    });
  };

  // Process incoming dataRowUpdate call
  const updateDataRow = (mode, params) => {
    switch (mode) {
      case "add":
        return addRow(params);

      case "update":
        return updateRow(params);

      case "delete":
        return deleteRow(params);
    }
  };

  return [data, updateDataRow];
}

export default useData;
