import axios from "axios";

/**
 * If columns definition array of grid contains field which has lookupUrl property,
 * then REST call to get list and render lookup property.
 */

const processColumns = rawColumns => {
  return new Promise(async (resolve, reject) => {
    let columns = [];

    // Process each column
    for (const rawColumn of rawColumns) {
      if (rawColumn.lookupUrl && rawColumn.lookupField) {
        // REST call for getting data for lookup
        let lookupResult;
        try {
          lookupResult = await axios.get(
            process.env.REACT_APP_REST_URL + rawColumn.lookupUrl
          );
        } catch (error) {
          console.log(error);
          reject(rawColumns);
          return;
        }

        // Covert REST call result to lookup object for MaterialTable
        let lookup = {};
        lookupResult.data.forEach(
          lookupElement =>
            (lookup[lookupElement.id] = lookupElement[rawColumn.lookupField])
        );

        columns.push({ ...rawColumn, lookup });
      } else {
        // The column doesn't have lookup property, simply return it
        columns.push(rawColumn);
      }
    }
    resolve(columns);
  });
};

export default processColumns;
