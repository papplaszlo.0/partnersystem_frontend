import { useState, useEffect } from "react";
import processColumns from "./processColumns";

// HOOK for rendering MaterialTable column definition
function useColumns(rawColumns) {
  const [columns, setColumns] = useState(null);

  useEffect(() => {
    // Process columns
    processColumns(rawColumns)
      .then(setColumns)
      .catch(setColumns);
  }, [rawColumns]);

  return [columns];
}

export default useColumns;
