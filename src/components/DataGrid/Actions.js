import FilterList from "@material-ui/icons/FilterList";

// Render Actions attribute for MaterialTable grid component
const renderActions = (onRowSelect, onClick) => {
  if (onRowSelect) {
    return null;
  } else {
    return [
      // Filter option button definition
      {
        icon: FilterList,
        isFreeAction: true,
        tooltip: "Szűrés",
        onClick: onClick
      }
    ];
  }
};

export default renderActions;
