import React from "react";
import DataGrid from "../../components/DataGrid";

// List and manage locations
function Locations(props) {
  const columns = [
    {
      field: "name",
      title: "Település neve"
    }
  ];
  return (
    <DataGrid
      title="Települések"
      columns={columns}
      dataRoute="locations"
      onRowSelect={props.onRowSelect}
    />
  );
}

export default Locations;
