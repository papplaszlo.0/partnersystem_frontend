import React from "react";
import DataGrid from "../../components/DataGrid";

// List and manage company forms
function CompanyForms(props) {
  const columns = [
    { title: "Rövid megnevezés", field: "abbreviation" },
    { title: "Megnevezés:", field: "description" }
  ];

  return (
    <DataGrid
      title="Cégformák"
      columns={columns}
      dataRoute="companyForms"
      onRowSelect={props.onRowSelect}
    />
  );
}

export default CompanyForms;
