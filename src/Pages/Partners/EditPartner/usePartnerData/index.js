import { useState } from "react";

// Hook to store and manage from data values
function usePartnerData(defaultData) {
  const [data, setData] = useState(defaultData);

  // Function to change only one property of data object
  const changeData = (field, value) => {
    setData({ ...data, [field]: value });
  };

  return [data, changeData];
}

export default usePartnerData;
