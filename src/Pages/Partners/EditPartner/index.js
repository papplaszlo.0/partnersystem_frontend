import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import usePartnerData from "./usePartnerData";
import EditField from "./EditField";
import { withSnackbar } from "notistack";

function EditPartner(props) {
  // HOOK to store and manage partner data
  const [data, changeData] = usePartnerData(props.data);

  // Missing fileds list, to sing component with error
  const [fieldsWithError, setFieldsWithError] = useState([]);

  // Function to check the required input fields are filled
  const checkRequiredFields = () => {
    return new Promise((resolve, reject) => {
      let missingFields = [];

      props.columns.forEach(column => {
        if (column.required) {
          // If the column value required and not defined, add to the error list
          if (!data || !data[column.field]) missingFields.push(column.field);
        }
      });

      setFieldsWithError(missingFields);

      if (missingFields.length > 0) {
        reject();
      } else {
        resolve();
      }
    });
  };

  // Submit form
  const submitForm = () => {
    // Before submit the form, check the required fileds are filled
    checkRequiredFields()
      .then(() => {
        // All required data filled
        props.onEditingApproved(props.mode, data, props.data);
      })
      .catch(() => {
        /// There are empty required fields
        props.enqueueSnackbar("Hiba! Hiányosan kitöltött mezők!", {
          variant: "error"
        });
      });
  };

  return (
    <Dialog open>
      <DialogTitle>
        {props.mode === "update" ? "Partner módosítása" : "Partner létrehozása"}
      </DialogTitle>
      <DialogContent>
        <Grid container spacing={2}>
          {/* Show the imput fields, based on Grid columns definitaion */}
          {props.columns.map((columnData, key) => {
            return (
              <Grid item key={key}>
                <EditField
                  {...columnData}
                  value={
                    data && data[columnData.field] ? data[columnData.field] : ""
                  }
                  onChange={value => changeData(columnData.field, value)}
                  error={
                    // If the field in the filedsWithError list, sign the component with error
                    fieldsWithError.indexOf(columnData.field) === -1
                      ? false
                      : true
                  }
                />
              </Grid>
            );
          })}
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button color="primary" variant="contained" onClick={submitForm}>
          Mentés
        </Button>
        <Button
          variant="contained"
          onClick={() => props.onEditingCanceled(props.mode, data)}
        >
          Mégse
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default withSnackbar(EditPartner);
