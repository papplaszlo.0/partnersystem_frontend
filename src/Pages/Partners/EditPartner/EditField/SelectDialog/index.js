import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import SearchIcon from "@material-ui/icons/Search";
import TableDialog from "./TableDialog";

function SelectDialog(props) {
  // State for show dialog (if true, the dialog will be opened)
  const [showDialog, setShowDialog] = useState(false);

  // Store the caption of select comopnent. User can see this as value. The real value is props.value
  const [caption, setCaption] = useState(props.lookup[props.value]);

  // Event when value is selected in the dialog
  const onSelect = row => {
    // If lookup of grid doesn't contain the selected value, add it.
    // Usualy it is not definied, when the selected value is new record
    if (!props.lookup[row.id]) {
      props.lookup[row.id] = row[props.lookupField];
    }
    // Change the visible caption for user
    setCaption(row[props.lookupField]);
    props.onChange(row.id);
    setShowDialog(false);
  };

  return (
    <React.Fragment>
      <TableDialog
        {...props}
        open={showDialog}
        onSelect={onSelect}
        onClose={() => setShowDialog(false)}
      />
      <Grid container direction="row" justify="center" alignItems="flex-end">
        <Grid item>
          <TextField
            value={caption ? caption : ""}
            label={props.title}
            error={props.error}
          />
        </Grid>
        <Grid item>
          <IconButton onClick={() => setShowDialog(true)} size="small">
            <SearchIcon />
          </IconButton>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default SelectDialog;
