import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";

// Dialog componet with grid, to select value.
function TableDialog(props) {
  const [selectedRow, setSelectedRow] = useState(null);
  const GridComponent = props.selectComponent;

  return (
    <Dialog open={props.open}>
      <DialogContent>
        <GridComponent onRowSelect={setSelectedRow} />
      </DialogContent>
      <DialogActions>
        <Button
          color="primary"
          variant="contained"
          onClick={() => props.onSelect(selectedRow)}
          disabled={selectedRow ? false : true}
        >
          Kiválaszt
        </Button>
        <Button variant="contained" onClick={props.onClose}>
          Mégsem
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default TableDialog;
