import React from "react";
import TextField from "@material-ui/core/TextField";
import SelectDialog from "./SelectDialog";

// Component for input values
function EditField(props) {
  if (props.lookup) {
    // If the grid column is lookup filed, return with select dialog component
    return <SelectDialog {...props} />;
  } else {
    return (
      // In case of simple column, return text field input component
      <TextField
        label={props.title}
        value={props.value}
        onChange={event => props.onChange(event.target.value)}
        error={props.error}
      />
    );
  }
}

export default EditField;
