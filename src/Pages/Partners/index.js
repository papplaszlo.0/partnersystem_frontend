import React from "react";
import DataGrid from "../../components/DataGrid";
import EditPartner from "./EditPartner";
import { MTableEditRow } from "material-table";
import Locations from "../Locations";
import CompanyForms from "../CompanyForms";

// LIst and manage partners
function Partners() {
  const columns = [
    { title: "Cég neve", field: "companyName", required: true },
    {
      title: "Cégforma",
      field: "formID",
      lookupUrl: "companyForms", // Rest url to get lookup fields
      lookupField: "abbreviation",
      selectComponent: CompanyForms // Component to select value
    },
    { title: "Bankszámlaszám", field: "bankAccountNum" },
    { title: "Cégjegyzék szám", field: "registrationNumber" },
    { title: "Adószám", field: "taxNumber" },
    {
      title: "Település",
      field: "locationID",
      lookupUrl: "locations",
      lookupField: "name",
      required: true,
      selectComponent: Locations
    },
    { title: "Cím", field: "address" },
    { title: "Telefon", field: "phone" },
    { title: "Megjegyzés", field: "comment" }
  ];

  return (
    <DataGrid
      title="Partnerek"
      columns={columns}
      dataRoute="partners"
      components={{
        EditRow: props => {
          if (props.mode === "delete") {
            // In case of delete, use the default component
            return <MTableEditRow {...props} />;
          } else {
            // in case of add or edit, use own component
            return <EditPartner {...props} />;
          }
        }
      }}
    />
  );
}

export default Partners;
